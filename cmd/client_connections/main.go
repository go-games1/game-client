package main

import (
	"bufio"
	"encoding/gob"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/go-games1/multiplayer-networking/transport/packet"
	"gitlab.com/go-games1/multiplayer-networking/transport/session"
	"gitlab.com/go-games1/multiplayer-networking/transport/udp"
)

func main() {
	port := flag.Int("port", 4001, "port to be used on the transceiver")

	flag.Parse()

	gob.Register(session.Action(""))

	logger := log.New(os.Stdout, "", 0)

	serverAddress := packet.Address{
		Host: "127.0.0.1",
		Port: 4000,
	}

	tx, err := udp.NewTransceiver(logger, *port)
	if err != nil {
		log.Fatal(err)
	}

	packetsDispatcher := packet.NewChannel(tx)

	fmt.Println("Listening on address", "127.0.0.1:4001")

	go func() {
		reader := bufio.NewReader(os.Stdin)
		for {
			input, err := reader.ReadString('\n')
			if err != nil {
				log.Fatal(err)
			}
			input = strings.TrimSuffix(input, "\n")
			payload := packet.Payload{
				Action: input,
			}

			if input == "SESSION_REQ" {
				payload = packet.Payload{
					Action: session.Request,
				}
			}

			if input == "DISCONNECT_REQ" {
				payload = packet.Payload{
					Action: session.DisconnectRequest,
				}
			}

			// TODO: move hardcoded addresses to config
			if err := packetsDispatcher.Send(payload, serverAddress); err != nil {
				log.Fatal(err)
			}
		}
	}()

	for {
		p, err := packetsDispatcher.Listen()
		if err != nil {
			log.Fatal(err)
		}

		if action, ok := p.Payload.Action.(session.Action); ok {
			if action == session.ChallengeRequest {
				err := packetsDispatcher.Send(packet.Payload{
					Action: session.ChallengeResponse,
					Data:   p.Payload.Data,
				}, serverAddress)
				if err != nil {
					panic(err)
				}

				go func() {
					for {
						err := packetsDispatcher.Send(packet.Payload{
							Action: session.Tick,
							Data:   nil,
						}, serverAddress)
						if err != nil {
							panic(err)
						}

						time.Sleep(time.Second)
					}
				}()
			}
		}

		fmt.Printf("server> %v - %s\n", p.Payload.Action, string(p.Payload.Data))
	}
}
