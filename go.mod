module lab/game-client

go 1.20

require gitlab.com/go-games1/multiplayer-networking v0.0.0-00010101000000-000000000000

require github.com/juju/errors v1.0.0 // indirect

replace gitlab.com/go-games1/multiplayer-networking => ../multiplayer-network
