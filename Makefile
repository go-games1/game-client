PROJECT_NAME := game-client
FOLDER_BIN = $(CURDIR)/bin
GO_TEST=go test ./...

start:
	go run cmd/client/main.go

build:
	go build -o $(FOLDER_BIN)/$(PROJECT_NAME) $(CURDIR)/cmd/repl/main.go

test:
	$(GO_TEST) -coverprofile cover.out

test-race:
	$(GO_TEST) -race

mod:
	go mod tidy
